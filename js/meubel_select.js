AFRAME.registerComponent('select_meubel', {

    init: function () {
        let butNext = document.getElementById("js--buttonNext");
        let butPrev = document.getElementById("js--buttonPrev");
        let meubel = document.getElementById("js--meubel");
        let product = document.getElementById("js--product-beschrijving");

        var click_sound = document.getElementById("js--click-sound");

        const kallaxBox = document.getElementById("js--kallax-clickbox");
        const hyllisBox = document.getElementById("js--hyllis-clickbox");

        const clickBox = document.getElementById("js--clickbox");
        const clickBoxText = document.getElementById("js--clickbox-text");

        const kallax = "url(../../assets/kallax/kallax.glb";
        const knarrevik = "url(../../assets/kallax/knarrevik.glb";
        const malm = "url(../../assets/kallax/malm.glb";
        const hyllis = "url(../../assets/hyllis/hyllis_compleet.glb";

        const kallaxInfo = "Kallax, 1 persoon, 1 uur, **";
        const knarrevikInfo = "Knarrevik, 1 persoon, kwartier, *";
        const malmInfo = "Malm, 2 personen, 1.5 uur, **";
        const hyllisInfo = "Hyllis, 2 personen, 3 kwartier, **"

        var meubels = [kallax, knarrevik, malm, hyllis]
        var info = [kallaxInfo, knarrevikInfo, malmInfo, hyllisInfo]

        // MEUBEL SELECT
        function nextItem() {
            var indexNo = meubels.indexOf(meubel.getAttribute('gltf-model'));
            if (meubels.length === indexNo+1 ) {
                return meubels[0]
            } else {
                return meubels[indexNo + 1]
            }
        };

        function prevItem() {
            var indexNo = meubels.indexOf(meubel.getAttribute('gltf-model'));
            if (indexNo == 0) {
                return meubels[meubels.length - 1 ]
            } else {
                return meubels[indexNo - 1]
            }
        };

        // Ga naar volgende meubel
        butNext.addEventListener('click', function(e) {
            //console.log("test next");
            click_sound.components.sound.playSound();
            meubel.setAttribute('gltf-model', nextItem());
            if(meubel.getAttribute('gltf-model') == hyllis){
                meubel.setAttribute('scale', "0.1 0.1 0.1")
                voegObjectToe(hyllisBox);
            }
            else {
                meubel.setAttribute('scale', "1.5 1.5 1.5")
                verwijderObject(hyllisBox);
            }
            product.setAttribute('value', info[meubels.indexOf(meubel.getAttribute('gltf-model'))]);
            verwijderObject(clickBox);
            voegObjectToe(meubel);

            if (meubel.getAttribute('gltf-model') != kallax){ 
                verwijderObject(kallaxBox);
            }else{
                voegObjectToe(kallaxBox);
            }
        });

        // Ga naar vorige meubel
        butPrev.addEventListener('click', function(e) {
            //console.log("test previous");
            click_sound.components.sound.playSound();
            meubel.setAttribute('gltf-model', prevItem());
            if(meubel.getAttribute('gltf-model') == hyllis){
                meubel.setAttribute('scale', "0.1 0.1 0.1")
                voegObjectToe(hyllisBox);
            }
            else {
                meubel.setAttribute('scale', "1.5 1.5 1.5")
                verwijderObject(hyllisBox);
            }
            product.setAttribute('value', info[meubels.indexOf(meubel.getAttribute('gltf-model'))]);
            verwijderObject(clickBox);
            voegObjectToe(meubel);

            if (meubel.getAttribute('gltf-model') != kallax){ 
                verwijderObject(kallaxBox);
            }else{
                voegObjectToe(kallaxBox);
            }
        });

        meubel.addEventListener('click', function(e) {
            if (meubel.getAttribute('gltf-model') != kallax || meubel.getAttribute('gltf-model') != hyllis ) { 
                voegObjectToe(clickBox);
                clickBox.setAttribute('opacity', "1");
                clickBox.setAttribute('position', "0 1.49 -6");
                clickBoxText.setAttribute('value', "Stappen niet beschikbaar");
                verwijderObject(meubel);
                clickBox.classList.remove("clickable");
            }else{
                verwijderObject(clickBox);
            }
        });

        function voegObjectToe(object){
            object.setAttribute("visible", "true");
            object.classList.add("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition < -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition + 100, z: currentZPosition});
            }
        }
    
        function verwijderObject(object){
            object.setAttribute("visible", "false");
            object.classList.remove("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition > -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition - 100, z: currentZPosition});
            }
        }
    }
});