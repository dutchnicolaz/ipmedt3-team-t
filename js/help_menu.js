AFRAME.registerComponent('help_menu', {

    init: function () {

        let helpMenu = document.getElementById("js--help-menu");
        const helpButton = document.getElementById("js--help-button");
        let helpClose = document.getElementById("js--help-menu-sluiten");
        var click_sound = document.getElementById("js--click-sound");

        const helpOnderwerpen = document.querySelectorAll(".js--help-menu-onderwerp");

        const antwoordenScherm = document.getElementById("js--antwoorden-scherm");
        const antwoordenKruisje = document.getElementById("js--sluiten-antwoorden-scherm");

        const terugKnop = document.getElementById("js--terug-naar-menu");

        const antwoord1 = document.getElementById("js--antwoord-1");
        const antwoord2 = document.getElementById("js--antwoord-2");
        const antwoord3 = document.getElementById("js--antwoord-3");
        const antwoord4 = document.getElementById("js--antwoord-4");
        const antwoord5 = document.getElementById("js--antwoord-5");

        function voegObjectToe(object){
            object.setAttribute("visible", "true");
            object.classList.add("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition < -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition + 100, z: currentZPosition});
            }
        }

        function verwijderObject(object){
            object.setAttribute("visible", "false");
            object.classList.remove("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition > -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition - 100, z: currentZPosition});
            }
        }

        
        helpButton.addEventListener('click', function(e) {
            click_sound.components.sound.playSound();
            voegObjectToe(helpMenu);
            helpMenu.setAttribute("position", "0 1.5 -5");
            helpClose.classList.add("clickable");
            helpOnderwerpen.forEach((vraag)=>{
                vraag.classList.add("clickable");
            })
        
        });

        helpClose.addEventListener('click', function(e){
            click_sound.components.sound.playSound();
            verwijderObject(helpMenu);
            helpMenu.setAttribute("position", "0 12 -5");
        })
        
        antwoordenKruisje.addEventListener('click', function(e){
            click_sound.components.sound.playSound();
            verwijderObject(antwoordenScherm);
            antwoordenScherm.setAttribute("position", "0 12 -1");
            antwoord1.setAttribute("visible", "false");
            antwoord2.setAttribute("visible", "false");
            antwoord3.setAttribute("visible", "false");
            antwoord4.setAttribute("visible", "false");
            antwoord5.setAttribute("visible", "false");
            antwoordenScherm.setAttribute("height", "3"); 
        }) 

        terugKnop.addEventListener('click', function(e){
            click_sound.components.sound.playSound();
            verwijderObject(antwoordenScherm);
            antwoordenScherm.setAttribute("position", "0 12 -1");
            antwoord1.setAttribute("visible", "false");
            antwoord2.setAttribute("visible", "false");
            antwoord3.setAttribute("visible", "false");
            antwoord4.setAttribute("visible", "false");
            antwoord5.setAttribute("visible", "false");
            antwoordenScherm.setAttribute("height", "3"); 
            voegObjectToe(helpMenu);
            helpMenu.setAttribute("position", "0 1.5 -5");
        }) 
        
        let show_antwoorden = () => {
            verwijderObject(helpMenu);
            helpMenu.setAttribute("position", "0 12 -5" );
            voegObjectToe(antwoordenScherm);
            antwoordenScherm.setAttribute("position", "0 2 -5")
        }


        helpOnderwerpen[1].addEventListener('click', function(e){
            click_sound.components.sound.playSound();
            antwoord1.setAttribute("visible", "true");
            show_antwoorden();
            antwoordenScherm.setAttribute("height", "2.5");
            antwoordenKruisje.setAttribute("position", '3.25 1.05 0.01');
            terugKnop.setAttribute("position", "-3.25 1 0.01");
        });

        helpOnderwerpen[2].addEventListener('click', function(e){
            click_sound.components.sound.playSound();
            antwoord2.setAttribute("visible", "true");
            show_antwoorden();
            antwoordenScherm.setAttribute("height", "4");
            antwoordenKruisje.setAttribute("position", '3.25 1.8 0.01');
            terugKnop.setAttribute("position", "-3.25 1.75 0.01");
        });

        helpOnderwerpen[3].addEventListener('click', function(e){
            click_sound.components.sound.playSound();
            antwoord3.setAttribute("visible", "true");
            show_antwoorden();
            antwoordenScherm.setAttribute("height", "3.5");
            antwoordenKruisje.setAttribute("position", '3.25 1.55 0.01');
            terugKnop.setAttribute("position", "-3.25 1.55 0.01");
        });

        helpOnderwerpen[4].addEventListener('click', function(e){
            click_sound.components.sound.playSound();
            antwoord4.setAttribute("visible", "true");
            show_antwoorden();
            antwoordenKruisje.setAttribute("position", '3.25 1.3 0.01');
            terugKnop.setAttribute("position", "-3.25 1.25 0.01");
        });

        helpOnderwerpen[5].addEventListener('click', function(e){
            click_sound.components.sound.playSound();
            antwoord5.setAttribute("visible", "true");
            show_antwoorden();antwoordenKruisje.setAttribute("position", '3.25 1.3 0.01');
            terugKnop.setAttribute("position", "-3.25 1.25 0.01");
        });
    }
});