AFRAME.registerComponent('bouw_kallax', {

    init: function () {

        const camera = document.getElementById("js--camera");

        // Kallax placeholder planken voorbeelden
        const phPlank1_1 = document.getElementById("js--placeholder-plank1.1");
        const phPlank2_1 = document.getElementById("js--placeholder-plank2.1");
        const phPlank4_1 = document.getElementById("js--placeholder-plank4.1");
        const phPlank3_1 = document.getElementById("js--placeholder-plank3.1");
        const phPlank4_2 = document.getElementById("js--placeholder-plank4.2");
        const phPlank2_2 = document.getElementById("js--placeholder-plank2.2");
        const phPlank1_2 = document.getElementById("js--placeholder-plank1.2");

        // Kallax placholder schroeven voorbeelden
        const phSchroefSet1 = document.getElementById("js--placeholder-schroefset1");
        const phSchroef1 = document.querySelectorAll(".placeholder-schroef1");
        const phSchroefSet2 = document.getElementById("js--placeholder-schroefset2");
        const phSchroef2 = document.querySelectorAll(".placeholder-schroef2");

        // Kallax placeholder pluggen voorbeelden
        const phPlugSet1 = document.getElementById("js--placeholder-plugset1");
        const phPlug1 = document.querySelectorAll(".placeholder-plug1");
        const phPlugSet2 = document.getElementById("js--placeholder-plugset2");
        const phPlug2 = document.querySelectorAll(".placeholder-plug2");
        const phPlugSet3 = document.getElementById("js--placeholder-plugset3");
        const phPlug3 = document.querySelectorAll(".placeholder-plug3");
        const phPlugSet4 = document.getElementById("js--placeholder-plugset4");
        const phPlug4 = document.querySelectorAll(".placeholder-plug4");
        const phPlugSet5 = document.getElementById("js--placeholder-plugset5");
        const phPlug5 = document.querySelectorAll(".placeholder-plug5");

        // Kallax planken
        const plank1_1 = document.getElementById("js--plank1.1");
        const plank2_1 = document.getElementById("js--plank2.1");
        const plank4_1 = document.getElementById("js--plank4.1");
        const plank3_1 = document.getElementById("js--plank3.1");
        const plank4_2 = document.getElementById("js--plank4.2");
        const plank2_2 = document.getElementById("js--plank2.2");
        const plank1_2 = document.getElementById("js--plank1.2");

        // Kallax schroeven
        const schroefSet1 = document.getElementById("js--schroefset1");
        const schroefSet2 = document.getElementById("js--schroefset2");

        // Kallax pluggen
        const plugSet1 = document.getElementById("js--plugset1");
        const plugSet2 = document.getElementById("js--plugset2");
        const plugSet3 = document.getElementById("js--plugset3");
        const plugSet4 = document.getElementById("js--plugset4");
        const plugSet5 = document.getElementById("js--plugset5");

        // Onderdelen die men pakt en in de hand houd
        const plank1 = document.getElementById("js--onderdeel-plank1");
        const plank2 = document.getElementById("js--onderdeel-plank2");
        const plank3 = document.getElementById("js--onderdeel-plank3");
        const plank4 = document.getElementById("js--onderdeel-plank4");
        const plug = document.getElementById("js--onderdeel-plug");
        const schroef = document.getElementById("js--onderdeel-schroef");

        // Kallax stappen van stappenscherm
        const stap0Knop = document.getElementById("js--stap0");
        const stap1Knop = document.getElementById("js--stap1");
        const stap2Knop = document.getElementById("js--stap2");
        const stap3Knop = document.getElementById("js--stap3");
        const stap4Knop = document.getElementById("js--stap4");
        const stap5Knop = document.getElementById("js--stap5");
        const stap6Knop = document.getElementById("js--stap6");
        const stap7Knop = document.getElementById("js--stap7");

        const trashcan = document.getElementById("js--trashcan");
        const butHome = document.getElementById("js--home");
        const butBack = document.getElementById("js--back");
        const kallax = document.getElementById("js--kallax");
        const meubelRotatieL = document.getElementById("js--meubel-rotatieL");
        const meubelRotatieR = document.getElementById("js--meubel-rotatieR");
        let inBeweging = false;
        const instructieStap = document.getElementById("js--instructie-stap");
        const instructieText = document.getElementById("js--instructie-text");

        // Geluiden
        var trash_sound = document.getElementById("js--trash-sound");
        var grab_sound = document.getElementById("js--grab-sound");

        let hold = null;

        // Prullenbak
        trashcan.addEventListener("click", function(e){
            leegHand(document.getElementById("js--onderdeel-" + hold));
            trash_sound.components.sound.playSound();
        });

        // Meubel rotatie linksom
        meubelRotatieL.addEventListener("click", function(e){
            if(inBeweging == false){
                inBeweging = true;
                let changeYaxis = kallax.getAttribute("rotation").y - 45;
                if(changeYaxis <= -360){
                    changeYaxis = 0;
                }
                kallax.setAttribute("rotation", {y: changeYaxis});
                setTimeout(function(){ inBeweging = false; }, 200);
            }
        });

        // Meubel rotatie rechtsom
        meubelRotatieR.addEventListener("click", function(e){
            if(inBeweging == false){
                inBeweging = true;
                let changeYaxis = kallax.getAttribute("rotation").y + 45;
                if(changeYaxis >= 360){
                    changeYaxis = 0;
                }
                kallax.setAttribute("rotation", {y: changeYaxis});
                setTimeout(function(){ inBeweging = false; }, 200);
                console.log(changeYaxis);
            }
        });

        // Home button - leeg hand en stop voice
        butHome.addEventListener("click", function(e){
            leegHand(document.getElementById("js--onderdeel-" + hold));
            responsiveVoice.cancel();
        })

        // Back button - Leeg hand en stop voice
        butBack.addEventListener("click", function(e){
            leegHand(document.getElementById("js--onderdeel-" + hold));
            responsiveVoice.cancel();
        })

        //Hand legen functie
        function leegHand(plank){
            if (hold != null ){
                const inHand = document.getElementById("js--hold");
                inHand.parentNode.removeChild(inHand);
                voegObjectToe(plank);
                hold = null;
            };
        }

        function voegObjectToe(object){
            object.setAttribute("visible", "true");
            object.classList.add("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition < -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition + 100, z: currentZPosition});
            }
        }

        function verwijderObject(object){
            object.setAttribute("visible", "false");
            object.classList.remove("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition > -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition - 100, z: currentZPosition});
            }
        }

        // Onderdeel pakken - voeg toe aan camera
        plank1.addEventListener("click", function(e){
            if (hold==null) {
                hold = "plank1";
                camera.innerHTML += '<a-entity id="js--hold" position="1 -.5 -1"><a-entity gltf-model="url(./assets/kallax/plank1.glb)" rotation="0 90 0"></a-entity><a-text value="Plank 1" position="-.45 .37 .5" align="center" width="1" font="./assets/fonts/Segoe_UI.fnt" fontImage="Segoe_UI.png"></a-text></a-entity>';
                verwijderObject(this);
                grab_sound.components.sound.playSound();
            }
        });

        plank2.addEventListener("click", function(e){
            if (hold==null) {
                hold = "plank2";
                camera.innerHTML += '<a-entity id="js--hold" position="1 -.5 -1"><a-entity gltf-model="url(./assets/kallax/plank2.glb)" rotation="0 90 0"></a-entity><a-text value="Plank 2" position="-.47 .37 .5" align="center" width="1" font="./assets/fonts/Segoe_UI.fnt" fontImage="Segoe_UI.png"></a-text></a-entity>';
                verwijderObject(this);
                grab_sound.components.sound.playSound();
            }
        });

        plank3.addEventListener("click", function(e){
            if (hold==null) {
                hold = "plank3";
                camera.innerHTML += '<a-entity id="js--hold" position="1 -.5 -1"><a-entity gltf-model="url(./assets/kallax/plank3.glb)" rotation="0 90 0"></a-entity><a-text value="Plank 3" position="-.47 .36 .5" align="center" width="1" font="./assets/fonts/Segoe_UI.fnt" fontImage="Segoe_UI.png"></a-text></a-entity>';
                verwijderObject(this);
                grab_sound.components.sound.playSound();
            }
        });

        plank4.addEventListener("click", function(e){
            if (hold==null) {
                hold = "plank4";
                camera.innerHTML += '<a-entity id="js--hold" position="1 -.5 -1"><a-entity gltf-model="url(./assets/kallax/plank4.glb)" rotation="0 90 0"></a-entity><a-text value="Plank 4" position="-.55 .35 .5" align="center" width="1" font="./assets/fonts/Segoe_UI.fnt" fontImage="Segoe_UI.png"></a-text></a-entity>';
                verwijderObject(this);
                grab_sound.components.sound.playSound();
            }
        });

        plug.addEventListener("click", function(e){
            if (hold==null) {
                hold = "plug";
                camera.innerHTML += '<a-entity id="js--hold" position=".5 -.5 -1"><a-entity gltf-model="url(./assets/kallax/houten_plug.glb)" rotation="0 90 0" scale="6 6 6"></a-entity><a-text value="Houten plug" position="-.42 .35 .5" align="center" width="1" font="./assets/fonts/Segoe_UI.fnt" fontImage="Segoe_UI.png"></a-text></a-entity>';
                verwijderObject(this);
                grab_sound.components.sound.playSound();
            }
        });

        schroef.addEventListener("click", function(e){
            if (hold==null) {
                verwijderObject(this);
                hold = "schroef";
                camera.innerHTML += '<a-entity id="js--hold" position=".5 -.5 -1"><a-entity gltf-model="url(./assets/kallax/schroef_kallax.glb)" rotation="0 90 0" scale="6 6 6"></a-entity><a-text value="Schroef" position="-.37 .35 .5" align="center" width="1" font="./assets/fonts/Segoe_UI.fnt" fontImage="Segoe_UI.png"></a-text></a-entity>';
                grab_sound.components.sound.playSound();
            }
        });

        // Plank placeholder voorbeeld
        phSchroefSet1.onmouseenter = (event) => {
            if (hold=="schroef") {
                schroefSet1.setAttribute("visible", "true");
                schroefSet1.setAttribute("animation", "property: position; from: 0 .5 0; to: 0 .2 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlank1_1.onmouseenter = (event) => {
            if (hold=="plank1") {
                plank1_1.setAttribute("visible", "true");
                plank1_1.setAttribute("animation", "property: position; from: 0 1 0; to: 0 .7 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlank2_1.onmouseenter = (event) => {
            if (hold=="plank2") {
                plank2_1.setAttribute("visible", "true");
                plank2_1.setAttribute("animation", "property: position; from: -.725 1.725 0; to: -.725 1.425 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlugSet1.onmouseenter = (event) => {
            if (hold=="plug") {
                plugSet1.setAttribute("visible", "true");
                plugSet1.setAttribute("animation", "property: position; from: 0 .5 0; to: 0 .2 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlank4_1.onmouseenter = (event) => {
            if (hold=="plank4") {
                plank4_1.setAttribute("visible", "true");
                plank4_1.setAttribute("animation", "property: position; from: 0 1.375 0; to: 0 1.075 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlugSet2.onmouseenter = (event) => {
            if (hold=="plug") {
                plugSet2.setAttribute("visible", "true");
                plugSet2.setAttribute("animation", "property: position; from: .5 0 0; to: .2 0 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlank3_1.onmouseenter = (event) => {
            if (hold=="plank3") {
                plank3_1.setAttribute("visible", "true");
                plank3_1.setAttribute("animation", "property: position; from: 1 .725 0; to: .7 .725 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlugSet3.onmouseenter = (event) => {
            if (hold=="plug") {
                plugSet3.setAttribute("visible", "true");
                plugSet3.setAttribute("animation", "property: position; from: 0 .5 0; to: 0 .2 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlank4_2.onmouseenter = (event) => {
            if (hold=="plank4") {
                plank4_2.setAttribute("visible", "true");
                plank4_2.setAttribute("animation", "property: position; from: 0 2.075 0; to: 0 1.775 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlugSet4.onmouseenter = (event) => {
            if (hold=="plug") {
                plugSet4.setAttribute("visible", "true");
                plugSet4.setAttribute("animation", "property: position; from: .5 0 0; to: .2 0 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlank2_2.onmouseenter = (event) => {
            if (hold=="plank2") {
                plank2_2.setAttribute("visible", "true");
                plank2_2.setAttribute("animation", "property: position; from: .725 1.725 0; to: .725 1.425 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlugSet5.onmouseenter = (event) => {
            if (hold=="plug") {
                plugSet5.setAttribute("visible", "true");
                plugSet5.setAttribute("animation", "property: position; from: 0 .5 0; to: 0 .2 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlank1_2.onmouseenter = (event) => {
            if (hold=="plank1") {
                plank1_2.setAttribute("visible", "true");
                plank1_2.setAttribute("animation", "property: position; from: 0 2.45 0; to: 0 2.15 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phSchroefSet2.onmouseenter = (event) => {
            if (hold=="schroef") {
                schroefSet2.setAttribute("visible", "true");
                schroefSet2.setAttribute("animation", "property: position; from: 0 .5 0; to: 0 .2 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        // Plank plaatsen
        phSchroefSet1.addEventListener("click", function(e){
            if (hold=="schroef") {
                stap0_5();
                leegHand(schroef);
                grab_sound.components.sound.playSound();
            }
        });

        phPlank1_1.addEventListener("click", function(e){
            if (hold=="plank1") {
                stap1();
                leegHand(plank1);
                grab_sound.components.sound.playSound();
            }
        });

        phPlank2_1.addEventListener("click", function(e){
            if (hold=="plank2") {
                stap2();
                leegHand(plank2);
                grab_sound.components.sound.playSound();
            }
        });

        phPlugSet1.addEventListener("click", function(e){
            if (hold=="plug") {
                stap2_5();
                leegHand(plug);
                grab_sound.components.sound.playSound();
            }
        });

        phPlank4_1.addEventListener("click", function(e){
            if (hold=="plank4") {
                stap3();
                leegHand(plank4);
                grab_sound.components.sound.playSound();
            }
        });

        phPlugSet2.addEventListener("click", function(e){
            if (hold=="plug") {
                stap3_5();
                leegHand(plug);
                grab_sound.components.sound.playSound();
            }
        });

        phPlank3_1.addEventListener("click", function(e){
            if (hold=="plank3") {
                stap4();
                leegHand(plank3);
                grab_sound.components.sound.playSound();
            }
        });

        phPlugSet3.addEventListener("click", function(e){
            if (hold=="plug") {
                stap4_5();
                leegHand(plug);
                grab_sound.components.sound.playSound();
            }
        });

        phPlank4_2.addEventListener("click", function(e){
            if (hold=="plank4") {
                stap5();
                leegHand(plank4);
                grab_sound.components.sound.playSound();
            }
        });

        phPlugSet4.addEventListener("click", function(e){
            if (hold=="plug") {
                stap5_5();
                leegHand(plug);
                grab_sound.components.sound.playSound();
            }
        });

        phPlank2_2.addEventListener("click", function(e){
            if (hold=="plank2") {
                stap6();
                leegHand(plank2);
                grab_sound.components.sound.playSound();
            }
        });

        phPlugSet5.addEventListener("click", function(e){
            if (hold=="plug") {
                stap6_5();
                leegHand(plug);
                grab_sound.components.sound.playSound();
            }
        });

        phPlank1_2.addEventListener("click", function(e){
            if (hold=="plank1") {
                stap7();
                leegHand(plank1);
                grab_sound.components.sound.playSound();
            }
        });

        phSchroefSet2.addEventListener("click", function(e){
            if (hold=="schroef") {
                stap7_5();
                leegHand(schroef);
                grab_sound.components.sound.playSound();
            }
        });

        // Stappen
        stap0Knop.addEventListener("click", function(e){
            phSchroef1.forEach((phSchroef1) => {
                voegObjectToe(phSchroef1);
            });
            voegObjectToe(phSchroefSet1);
            instructieStap.setAttribute("value", "Stap 1");
            instructieText.setAttribute("value", "Open de doos met \n onderdelen. \n\n Plaats de Schroef \n op de gemarkeerde \n plek.");
            uitlegAudio();
        });

        function stap0_5(){
            schroefSet1.setAttribute("visible", "true");
            schroefSet1.removeAttribute("animation");
            schroefSet1.setAttribute("position", "0 0 0");
            phSchroef1.forEach((phSchroef1) => {
                verwijderObject(phSchroef1);
            });
            verwijderObject(phSchroefSet1);
            voegObjectToe(phPlank1_1);
            instructieStap.setAttribute("value", "Stap 2");
            instructieText.setAttribute("value", "Pak Plank 1 en \n plaats deze op \n de gemarkeerde \n plek.");
            uitlegAudio();
        }

        function stap1(){
            plank1_1.setAttribute("visible", "true");
            plank1_1.removeAttribute("animation");
            plank1_1.setAttribute("position", "0 0 0");
            verwijderObject(phPlank1_1);
            voegObjectToe(phPlank2_1);
            instructieStap.setAttribute("value", "Stap 3");
            instructieText.setAttribute("value", "Pak Plank 2 en \n plaats deze op \n de gemarkeerde \n plek. \n\n Gebruik de Inbus- \n sleutel om de plank \n vast te schroeven.");
            uitlegAudio();
        }

        stap1Knop.addEventListener("click", function(e){
            stap0_5();
            stap1();
        });

        function stap2(){
            plank2_1.setAttribute("visible", "true");
            plank2_1.removeAttribute("animation");
            plank2_1.setAttribute("position", "-.725 .725 0");
            verwijderObject(phPlank2_1);
            phPlug1.forEach((phPlug1) => {
                voegObjectToe(phPlug1);
            });
            voegObjectToe(phPlugSet1);
            instructieStap.setAttribute("value", "Stap 4");
            instructieText.setAttribute("value", "Pak de Plug en \n plaats deze op \n de gemarkeerde \n plek. \n\n Gebruik de Hamer \n  om de pluggen \n erin te slaan.");
            uitlegAudio();
        }

        stap2Knop.addEventListener("click", function(e){
            stap0_5();
            stap1();
            stap2();
        });

        function stap2_5(){
            plugSet1.setAttribute("visible", "true");
            plugSet1.removeAttribute("animation");
            plugSet1.setAttribute("position", "0 0 0");
            phPlug1.forEach((phPlug1) => {
                verwijderObject(phPlug1);
            });
            verwijderObject(phPlugSet1);
            voegObjectToe(phPlank4_1);
            instructieStap.setAttribute("value", "Stap 5");
            instructieText.setAttribute("value", "Pak Plank 4 en \n plaats deze op \n de gemarkeerde \n plek.");
            uitlegAudio();
        }

        function stap3(){
            plank4_1.setAttribute("visible", "true");
            plank4_1.removeAttribute("animation");
            plank4_1.setAttribute("position", "0 .375 0");
            verwijderObject(phPlank4_1);
            phPlug2.forEach((phPlug2) => {
                voegObjectToe(phPlug2);
            });
            voegObjectToe(phPlugSet2);
            instructieStap.setAttribute("value", "Stap 6");
            instructieText.setAttribute("value", "Pak de Plug en \n plaats deze op \n de gemarkeerde \n plek. \n\n Gebruik de Hamer \n  om de pluggen \n erin te slaan.");
            uitlegAudio();
        }

        stap3Knop.addEventListener("click", function(e){
            stap0_5();
            stap1();
            stap2();
            stap2_5();
            stap3();
        });

        function stap3_5(){
            plugSet2.setAttribute("visible", "true");
            plugSet2.removeAttribute("animation");
            plugSet2.setAttribute("position", "0 0 0");
            phPlug2.forEach((phPlug2) => {
                verwijderObject(phPlug2);
            });
            verwijderObject(phPlugSet2);
            voegObjectToe(phPlank3_1);
            instructieStap.setAttribute("value", "Stap 7");
            instructieText.setAttribute("value", "Pak Plank 3 en \n plaats deze op \n de gemarkeerde \n plek.");
            uitlegAudio();
        }

        function stap4(){
            plank3_1.setAttribute("visible", "true");
            plank3_1.removeAttribute("animation");
            plank3_1.setAttribute("position", "0 .725 0");
            verwijderObject(phPlank3_1);
            phPlug3.forEach((phPlug3) => {
                voegObjectToe(phPlug3);
            });
            voegObjectToe(phPlugSet3);
            instructieStap.setAttribute("value", "Stap 8");
            instructieText.setAttribute("value", "Pak de Plug en \n plaats deze op \n de gemarkeerde \n plek. \n\n Gebruik de Hamer \n  om de pluggen \n erin te slaan.");
            uitlegAudio();
        }

        stap4Knop.addEventListener("click", function(e){
            stap0_5();
            stap1();
            stap2();
            stap2_5();
            stap3();
            stap3_5();
            stap4();
        });

        function stap4_5(){
            plugSet3.setAttribute("visible", "true");
            plugSet3.removeAttribute("animation");
            plugSet3.setAttribute("position", "0 0 0");
            phPlug3.forEach((phPlug3) => {
                verwijderObject(phPlug3);
            });
            verwijderObject(phPlugSet3);
            voegObjectToe(phPlank4_2);
            instructieStap.setAttribute("value", "Stap 9");
            instructieText.setAttribute("value", "Pak Plank 4 en \n plaats deze op \n de gemarkeerde \n plek.");
            uitlegAudio();
        }

        function stap5(){
            plank4_2.setAttribute("visible", "true");
            plank4_2.removeAttribute("animation");
            plank4_2.setAttribute("position", "0 1.075 0");
            verwijderObject(phPlank4_2);
            phPlug4.forEach((phPlug4) => {
                voegObjectToe(phPlug4);
            });
            voegObjectToe(phPlugSet4);
            instructieStap.setAttribute("value", "Stap 10");
            instructieText.setAttribute("value", "Pak de Plug en \n plaats deze op \n de gemarkeerde \n plek. \n\n Gebruik de Hamer \n  om de pluggen \n erin te slaan.");
            uitlegAudio();
        }

        stap5Knop.addEventListener("click", function(e){
            stap0_5();
            stap1();
            stap2();
            stap2_5();
            stap3();
            stap3_5();
            stap4();
            stap4_5();
            stap5();
        });

        function stap5_5(){
            plugSet4.setAttribute("visible", "true");
            plugSet4.removeAttribute("animation");
            plugSet4.setAttribute("position", "0 0 0");
            phPlug4.forEach((phPlug4) => {
                verwijderObject(phPlug4);
            });
            verwijderObject(phPlugSet4);
            voegObjectToe(phPlank2_2);
            instructieStap.setAttribute("value", "Stap 11");
            instructieText.setAttribute("value", "Pak Plank 2 en \n plaats deze op \n de gemarkeerde \n plek. \n\n Gebruik de Inbus- \n sleutel om de plank \n vast te schroeven.");
            uitlegAudio();
        }

        function stap6(){
            plank2_2.setAttribute("visible", "true");
            plank2_2.removeAttribute("animation");
            plank2_2.setAttribute("position", ".725 .725 0");
            verwijderObject(phPlank2_2);
            phPlug5.forEach((phPlug5) => {
                voegObjectToe(phPlug5);
            });
            voegObjectToe(phPlugSet5);
            instructieStap.setAttribute("value", "Stap 12");
            instructieText.setAttribute("value", "Pak de Plug en \n plaats deze op \n de gemarkeerde \n plek. \n\n Gebruik de Hamer \n  om de pluggen \n erin te slaan.");
            uitlegAudio();
        }

        stap6Knop.addEventListener("click", function(e){
            stap0_5();
            stap1();
            stap2();
            stap2_5();
            stap3();
            stap3_5();
            stap4();
            stap4_5();
            stap5();
            stap5_5();
            stap6();
        });

        function stap6_5(){
            plugSet5.setAttribute("visible", "true");
            plugSet5.removeAttribute("animation");
            plugSet5.setAttribute("position", "0 0 0");
            phPlug5.forEach((phPlug5) => {
                verwijderObject(phPlug5);
            });
            verwijderObject(phPlugSet5);
            voegObjectToe(phPlank1_2);
            instructieStap.setAttribute("value", "Stap 13");
            instructieText.setAttribute("value", "Pak Plank 1 en \n plaats deze op \n de gemarkeerde \n plek.");
            uitlegAudio();
        }

        function stap7(){
            plank1_2.setAttribute("visible", "true");
            plank1_2.removeAttribute("animation");
            plank1_2.setAttribute("position", "0 1.45 0");
            verwijderObject(phPlank1_2);
            phSchroef2.forEach((phSchroef2) => {
                voegObjectToe(phSchroef2);
            });
            voegObjectToe(phSchroefSet2);
            instructieStap.setAttribute("value", "Stap 14");
            instructieText.setAttribute("value", "Pak de Schroef en \n plaats deze op \n de gemarkeerde plek. \n\n Gebruik de Inbus- \n sleutel om de plank \n vast te schroeven.");
            uitlegAudio();
        }

        function stap7_5(){
            schroefSet2.setAttribute("visible", "true");
            schroefSet2.removeAttribute("animation");
            schroefSet2.setAttribute("position", "0 0 0");
            phSchroef2.forEach((phSchroef2) => {
                verwijderObject(phSchroef2);
            });
            verwijderObject(phSchroefSet2);
            instructieStap.setAttribute("value", "KLAAR!");
            instructieText.setAttribute("value", "Gefeliciteerd! \n\n Je hebt succesvol \n de Kallax kast \n in elkaar gezet. \n\n Nu nog voor \n het echie ;)");
            kallax.setAttribute("animation", "property: position; from: 0 .045 -3; to: 0 .545 -3; loop: 1; dir: alternate; dur: 1000; easing: easeInOutQuad;");
            kallax.setAttribute("animation__2", "property: rotation; from: 0 0 0; to: 0 360 0; dur: 2000; easing: easeInOutQuad;");
            setTimeout(function(){ kallax.removeAttribute("animation"); kallax.removeAttribute("animation__2");}, 2000);
            uitlegAudio();
        }

        stap7Knop.addEventListener("click", function(e){
            stap0_5();
            stap1();
            stap2();
            stap2_5();
            stap3();
            stap3_5();
            stap4();
            stap4_5();
            stap5();
            stap5_5();
            stap6();
            stap6_5();
            stap7();
            stap7_5();
        });

        // Spraakfunctie
        uitlegAudio = () => {
            console.log("Stap uitleg");
            responsiveVoice.speak(String(instructieText.getAttribute("value")), "Dutch Male", {volume: 0.45});
        };
    }
});