AFRAME.registerComponent('bouw_hyllis', {

    init: function () {

        const camera = document.getElementById("js--camera");

        // Hyllis placeholder planken voorbeelden
        const phPlankSet1 = document.getElementById("ph-hyllis-plankset1");
        const phPlank1 = document.querySelectorAll(".ph-hyllis-plank1");

        //Hyllis placeholder poten voorbeelden
        const phPootSet1 = document.getElementById("ph-hyllis-pootset1");
        const phPoot1 = document.querySelectorAll(".ph-hyllis-poot1");
        const phPootSet2 = document.getElementById("ph-hyllis-pootset2");
        const phPoot2 = document.querySelectorAll(".ph-hyllis-poot2");

        //Hyllis placeholder schroeven voorbeelden
        const phSchroefSet1 = document.getElementById("ph-hyllis-schroefset1");
        const phSchroef1 = document.querySelectorAll(".ph-hyllis-schroef1");
        const phSchroefSet2 = document.getElementById("ph-hyllis-schroefset2");
        const phSchroef2 = document.querySelectorAll(".ph-hyllis-schroef2");
        const phSchroefSet3 = document.getElementById("ph-hyllis-schroefset3");
        const phSchroef3 = document.querySelectorAll(".ph-hyllis-schroef3");
        const phSchroefSet4 = document.getElementById("ph-hyllis-schroefset4");
        const phSchroef4 = document.querySelectorAll(".ph-hyllis-schroef4");

        //Hyllis placeholder cover voorbeelden
        const phCoverSet1 = document.getElementById("ph-hyllis-coverset1");
        const phCover1 = document.querySelectorAll(".ph-hyllis-cover1");

        //Hyllis poten
        const pootSet1 = document.getElementById("hyllis-pootset1");
        const pootSet2 = document.getElementById("hyllis-pootset2");

        //Hyllis planken
        const plankSet1 = document.getElementById("hyllis-plankset1");

        //Hyllis schroeven
        const schroefSet1 = document.getElementById("hyllis-schroefset1");
        const schroefSet2_1 = document.getElementById("hyllis-schroefset2.1");
        const schroefSet2_2 = document.getElementById("hyllis-schroefset2.2");
        const schroefSet3_1 = document.getElementById("hyllis-schroefset3.1");
        const schroefSet3_2 = document.getElementById("hyllis-schroefset3.2");
        const schroefSet4 = document.getElementById("hyllis-schroefset4");

        //Hyllis cover
        const coverSet1 = document.getElementById("hyllis-coverset1");

        // Onderdelen die men pakt en in de hand houd
        const poot = document.getElementById("js--hyllis-poot");
        const plank = document.getElementById("js--hyllis-plank");
        const schroef = document.getElementById("js--hyllis-schroef");
        const cover = document.getElementById("js--hyllis-cover");

        let hold = null;

        // Stappen in stappenscherm
        const stap0Knop = document.getElementById("js--stap0-hyllis");
        const stap1Knop = document.getElementById("js--stap1-hyllis");
        const stap2Knop = document.getElementById("js--stap2-hyllis");
        const stap3Knop = document.getElementById("js--stap3-hyllis");
        const stap4Knop = document.getElementById("js--stap4-hyllis");
        const stap5Knop = document.getElementById("js--stap5-hyllis");
        const stap6Knop = document.getElementById("js--stap6-hyllis");
        const stap7Knop = document.getElementById("js--stap7-hyllis");

        const trashcan = document.getElementById("js--trashcan-hyllis");
        const butHome = document.getElementById("js--home-hyllis");
        const butBack = document.getElementById("js--back-hyllis");
        const hyllis = document.getElementById("js--hyllis");
        const instructieStap = document.getElementById("js--instructie-stap-hyllis");
        const instructieText = document.getElementById("js--instructie-text-hyllis");
        const ibPlug = document.getElementById("js--ib-plug");

        // Geluiden
        var trash_sound = document.getElementById("js--trash-sound");
        var grab_sound = document.getElementById("js--grab-sound");


        // Prullenbak
        trashcan.addEventListener("click", function(e){
            leegHand(document.getElementById("js--hyllis-" + hold));
            trash_sound.components.sound.playSound();
        });

        // Home button - leeg hand en stop voice
        butHome.addEventListener("click", function(e){
            leegHand(document.getElementById("js--hyllis-" + hold));
            responsiveVoice.cancel();
        })

        // Back button - Leeg hand en stop voice
        butBack.addEventListener("click", function(e){
            leegHand(document.getElementById("js--hyllis-" + hold));
            responsiveVoice.cancel();
        })

        //Hand legen functie
        function leegHand(plank){
            if (hold != null ){
                const inHand = document.getElementById("js--hold");
                inHand.parentNode.removeChild(inHand);
                voegObjectToe(plank);
                hold = null;
            };
        }

        // Onderdeel pakken - voeg toe aan camera
        poot.addEventListener("click", function(e){
            if (hold==null) {
                hold = "poot";
                camera.innerHTML += '<a-entity id="js--hold" position="1 -.5 -1"><a-entity gltf-model="url(./assets/hyllis/hyllis_poot_1.glb)" position="-.1 -2.5 1" rotation="0 0 0" scale=".1 .1 .1"></a-entity><a-text value="Poot" position="-.45 .37 .5" align="center" width="1" font="./assets/fonts/Segoe_UI.fnt" fontImage="Segoe_UI.png"></a-text></a-entity>';
                verwijderObject(this);
                grab_sound.components.sound.playSound();
            }
        });

        plank.addEventListener("click", function(e){
            if (hold==null) {
                hold = "plank";
                camera.innerHTML += '<a-entity id="js--hold" position="1 -.5 -1"><a-entity gltf-model="url(./assets/hyllis/hyllis_plank_1.glb)" position=".2 -.5 .1" rotation="-90 0 0" scale=".1 .1 .1"></a-entity><a-text value="Plank" position="-.47 .37 .5" align="center" width="1" font="./assets/fonts/Segoe_UI.fnt" fontImage="Segoe_UI.png"></a-text></a-entity>';
                verwijderObject(this);
                grab_sound.components.sound.playSound();
            }
        });

        schroef.addEventListener("click", function(e){
            if (hold==null) {
                hold = "schroef";
                camera.innerHTML += '<a-entity id="js--hold" position="1 -.5 -1"><a-entity gltf-model="url(./assets/hyllis/schroef_HYLLIS.glb)" position="0 -.25 0" scale=".5 .5 .5"></a-entity><a-text value="Schroef" position="-.47 .36 .5" align="center" width="1" font="./assets/fonts/Segoe_UI.fnt" fontImage="Segoe_UI.png"></a-text></a-entity>';
                verwijderObject(this);
                grab_sound.components.sound.playSound();
            }
        });

        cover.addEventListener("click", function(e){
            if (hold==null) {
                hold = "cover";
                camera.innerHTML += '<a-entity id="js--hold" position="1 -.5 -1"><a-entity gltf-model="url(./assets/hyllis/plastic_cover.glb)" position="0 -.21 0" rotation="0 180 0" scale=".5 .5 .5"></a-entity><a-text value="Plastic cover" position="-.55 .35 .5" align="center" width="1" font="./assets/fonts/Segoe_UI.fnt" fontImage="Segoe_UI.png"></a-text></a-entity>';
                verwijderObject(this);
                grab_sound.components.sound.playSound();
            }
        });

        // Onderdeel placeholder voorbeeld
        phSchroefSet1.onmouseenter = (event) => {
            if (hold=="schroef") {
                schroefSet1.setAttribute("visible", "true");
                schroefSet1.setAttribute("animation", "property: position; from: -.5 0 0; to: -.2 0 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPootSet1.onmouseenter = (event) => {
            if (hold=="poot") {
                pootSet1.setAttribute("visible", "true");
                pootSet1.setAttribute("animation", "property: position; from: -.5 0 0; to: -.2 0 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPlankSet1.onmouseenter = (event) => {
            if (hold=="plank") {
                plankSet1.setAttribute("visible", "true");
                plankSet1.setAttribute("animation", "property: position; from: -.5 0 0; to: -.2 0 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phSchroefSet2.onmouseenter = (event) => {
            if (hold=="schroef") {
                schroefSet2_1.setAttribute("visible", "true");
                schroefSet2_1.setAttribute("animation", "property: position; from: 0 0 -.5; to: 0 0 -.2; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
                schroefSet2_2.setAttribute("visible", "true");
                schroefSet2_2.setAttribute("animation", "property: position; from: 0 0 .5; to: 0 0 .2; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phPootSet2.onmouseenter = (event) => {
            if (hold=="poot") {
                pootSet2.setAttribute("visible", "true");
                pootSet2.setAttribute("animation", "property: position; from: -.5 0 0; to: -.2 0 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phSchroefSet3.onmouseenter = (event) => {
            if (hold=="schroef") {
                schroefSet3_1.setAttribute("visible", "true");
                schroefSet3_1.setAttribute("animation", "property: position; from: 0 0 -.5; to: 0 0 -.2; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
                schroefSet3_2.setAttribute("visible", "true");
                schroefSet3_2.setAttribute("animation", "property: position; from: 0 0 .5; to: 0 0 .2; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phSchroefSet4.onmouseenter = (event) => {
            if (hold=="schroef") {
                schroefSet4.setAttribute("visible", "true");
                schroefSet4.setAttribute("animation", "property: position; from: -.5 0 0; to: -.2 0 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        phCoverSet1.onmouseenter = (event) => {
            if (hold=="cover") {
                coverSet1.setAttribute("visible", "true");
                coverSet1.setAttribute("animation", "property: position; from: 0 -.5 0; to: 0 -.2 0; dir: alternate; loop: true; dur: 1000; easing: easeInOutQuad;");
            }
        };

        // Onderdeel plaatsen
        phSchroefSet1.addEventListener("click", function(e){
            if (hold=="schroef") {
                stap1();
                leegHand(schroef);
                grab_sound.components.sound.playSound();
            }
        });

        phPootSet1.addEventListener("click", function(e){
            if (hold=="poot") {
                stap2();
                leegHand(poot);
                grab_sound.components.sound.playSound();
            }
        });

        phPlankSet1.addEventListener("click", function(e){
            if (hold=="plank") {
                stap3();
                leegHand(plank);
                grab_sound.components.sound.playSound();
            }
        });

        phSchroefSet2.addEventListener("click", function(e){
            if (hold=="schroef") {
                stap4();
                leegHand(schroef);
                grab_sound.components.sound.playSound();
            }
        });

        phPootSet2.addEventListener("click", function(e){
            if (hold=="poot") {
                stap5();
                leegHand(poot);
                grab_sound.components.sound.playSound();
            }
        });

        phSchroefSet3.addEventListener("click", function(e){
            if (hold=="schroef") {
                stap6();
                leegHand(schroef);
                grab_sound.components.sound.playSound();
            }
        });

        phSchroefSet4.addEventListener("click", function(e){
            if (hold=="schroef") {
                stap7();
                leegHand(schroef);
                grab_sound.components.sound.playSound();
            }
        });

        phCoverSet1.addEventListener("click", function(e){
            if (hold=="cover") {
                stap8();
                leegHand(cover);
                grab_sound.components.sound.playSound();
            }
        });

        // Stappen
        stap0Knop.addEventListener("click", function(e){
            phSchroef1.forEach((phSchroef1) => {
                voegObjectToe(phSchroef1);
            });
            voegObjectToe(phSchroefSet1);
            instructieStap.setAttribute("value", "Stap 1");
            instructieText.setAttribute("value", "Open de doos met \n onderdelen. \n\n Plaats de Schroef \n op de gemarkeerde \n plek.");
            uitlegAudio();

            hyllis.setAttribute('rotation', '0 45 -90');
            hyllis.setAttribute('position', "-1 0.28 -2");
        });

        function stap1(){
            schroefSet1.setAttribute("visible", "true");
            schroefSet1.removeAttribute("animation");
            schroefSet1.setAttribute("position", "0 0 0");
            phSchroef1.forEach((phSchroef1) => {
                verwijderObject(phSchroef1);
            });
            verwijderObject(phSchroefSet1);
            phPoot1.forEach((phPoot1) => {
                voegObjectToe(phPoot1);
            });
            voegObjectToe(phPootSet1);
            instructieStap.setAttribute("value", "Stap 2");
            instructieText.setAttribute("value", "Pak de poot en \n plaats deze op \n de gemarkeerde \n plek.");
            uitlegAudio();
            hyllis.setAttribute('rotation', '0 45 -90');
            hyllis.setAttribute('position', "-1 0.28 -2");
        }

        stap1Knop.addEventListener("click", function(e){
            stap1();
        });

        function stap2(){
            pootSet1.setAttribute("visible", "true");
            pootSet1.removeAttribute("animation");
            pootSet1.setAttribute("position", "0 0 0");
            phPoot1.forEach((phPoot1) => {
                verwijderObject(phPoot1);
            });
            verwijderObject(phPootSet1);
            phPlank1.forEach((phPlank1) => {
                voegObjectToe(phPlank1);
            });
            voegObjectToe(phPlankSet1);
            instructieStap.setAttribute("value", "Stap 3");
            instructieText.setAttribute("value", "Pak de plank en \n plaats deze op \n de gemarkeerde \n plek.");
            uitlegAudio();
        }

        stap2Knop.addEventListener("click", function(e){
            stap1();
            stap2();
        });

        function stap3(){
            plankSet1.setAttribute("visible", "true");
            plankSet1.removeAttribute("animation");
            plankSet1.setAttribute("position", "0 0 0");
            phPlank1.forEach((phPlank1) => {
                verwijderObject(phPlank1);
            });
            verwijderObject(phPlankSet1);
            phSchroef2.forEach((phSchroef2) => {
                voegObjectToe(phSchroef2);
            });
            voegObjectToe(phSchroefSet2);
            instructieStap.setAttribute("value", "Stap 4");
            instructieText.setAttribute("value", "Pak de schroef en \n plaats deze op \n de gemarkeerde \n plek.");
            uitlegAudio();
        }

        stap3Knop.addEventListener("click", function(e){
            stap1();
            stap2();
            stap3();
        });

        function stap4(){
            schroefSet2_1.setAttribute("visible", "true");
            schroefSet2_1.removeAttribute("animation");
            schroefSet2_1.setAttribute("position", "0 0 0");
            schroefSet2_2.setAttribute("visible", "true");
            schroefSet2_2.removeAttribute("animation");
            schroefSet2_2.setAttribute("position", "0 0 0");
            phSchroef2.forEach((phSchroef2) => {
                verwijderObject(phSchroef2);
            });
            verwijderObject(phSchroefSet2);
            phPoot2.forEach((phPoot2) => {
                voegObjectToe(phPoot2);
            });
            voegObjectToe(phPootSet2);
            instructieStap.setAttribute("value", "Stap 5");
            instructieText.setAttribute("value", "Pak de poot en \n plaats deze op \n de gemarkeerde \n plek.");
            uitlegAudio();
        }

        stap4Knop.addEventListener("click", function(e){
            stap1();
            stap2();
            stap3();
            stap4();
        });

        function stap5(){
            pootSet2.setAttribute("visible", "true");
            pootSet2.removeAttribute("animation");
            pootSet2.setAttribute("position", "0 0 0");
            phPoot2.forEach((phPoot2) => {
                verwijderObject(phPoot2);
            });
            verwijderObject(phPootSet2);
            phSchroef3.forEach((phSchroef3) => {
                voegObjectToe(phSchroef3);
            });
            voegObjectToe(phSchroefSet3);
            instructieStap.setAttribute("value", "Stap 6");
            instructieText.setAttribute("value", "Pak de schroef en \n plaats deze op \n de gemarkeerde \n plek.");
            uitlegAudio();
        }

        stap5Knop.addEventListener("click", function(e){
            stap1();
            stap2();
            stap3();
            stap4();
            stap5();
        });

        function stap6(){
            schroefSet3_1.setAttribute("visible", "true");
            schroefSet3_1.removeAttribute("animation");
            schroefSet3_1.setAttribute("position", "0 0 0");
            schroefSet3_2.setAttribute("visible", "true");
            schroefSet3_2.removeAttribute("animation");
            schroefSet3_2.setAttribute("position", "0 0 0");
            phSchroef3.forEach((phSchroef3) => {
                verwijderObject(phSchroef3);
            });
            verwijderObject(phSchroefSet3);
            phSchroef4.forEach((phSchroef4) => {
                voegObjectToe(phSchroef4);
            });
            voegObjectToe(phSchroefSet4);
            instructieStap.setAttribute("value", "Stap 7");
            instructieText.setAttribute("value", "Pak de schroef en \n plaats deze op \n de gemarkeerde \n plek.");
            uitlegAudio();
        }

        stap6Knop.addEventListener("click", function(e){
            stap1();
            stap2();
            stap3();
            stap4();
            stap5();
            stap6();
        });

        function stap7(){
            schroefSet4.setAttribute("visible", "true");
            schroefSet4.removeAttribute("animation");
            schroefSet4.setAttribute("position", "0 0 0");
            phSchroef4.forEach((phSchroef4) => {
                verwijderObject(phSchroef4);
            });
            verwijderObject(phSchroefSet4);
            phCover1.forEach((phCover1) => {
                voegObjectToe(phCover1);
            });
            voegObjectToe(phCoverSet1);
            instructieStap.setAttribute("value", "Stap 8");
            instructieText.setAttribute("value", "Pak de plastic \n cover en plaats \n deze op de \n gemarkeerde plek.");
            uitlegAudio();
        }

        stap7Knop.addEventListener("click", function(e){
            stap1();
            stap2();
            stap3();
            stap4();
            stap5();
            stap6();
            stap7();
        });

        function stap8(){
            coverSet1.setAttribute("visible", "true");
            coverSet1.removeAttribute("animation");
            coverSet1.setAttribute("position", "0 0 0");
            phCover1.forEach((phCover1) => {
                verwijderObject(phCover1);
            });
            verwijderObject(phCoverSet1);
            instructieStap.setAttribute("value", "KLAAR!");
            instructieText.setAttribute("value", "Gefeliciteerd! \n\n Je hebt succesvol \n de Hyllis kast \n in elkaar gezet. \n\n Nu nog voor \n het echie ;)");
            hyllis.setAttribute("animation", "property: position; from: 0 .045 -3; to: 0 .545 -3; loop: 1; dir: alternate; dur: 1000; easing: easeInOutQuad;");
            hyllis.setAttribute("animation__2", "property: rotation; from: 0 90 0; to: 0 450 0; dur: 2000; easing: easeInOutQuad;");
            setTimeout(function(){ hyllis.removeAttribute("animation"); hyllis.removeAttribute("animation__2");}, 2000);
            uitlegAudio();
        }

        // Spraakfunctie
        uitlegAudio = () => {
            console.log("Stap uitleg");
            responsiveVoice.speak(String(instructieText.getAttribute("value")), "Dutch Male", {volume: 0.45});
        };

        // Objecten toevoegen/verwijderen
        function voegObjectToe(object){
            object.setAttribute("visible", "true");
            object.classList.add("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition < -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition + 100, z: currentZPosition});
            }
        }

        function verwijderObject(object){
            object.setAttribute("visible", "false");
            object.classList.remove("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition > -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition - 100, z: currentZPosition});
            }
        }
    }
});