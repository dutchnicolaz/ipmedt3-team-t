AFRAME.registerComponent('start_applicatie', {

    init: function () {

        let startElements = document.querySelectorAll(".main");
        let sound_icon = document.getElementById("js--sound-icon");
        var background_sound = document.getElementById("js--background-sound");

        background_sound.components.sound.playSound();

        startElements.forEach((starElement) => {
            verwijderObject(starElement);
        })

        function voegObjectToe(object){
            object.setAttribute("visible", "true");
            object.classList.add("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition < -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition + 100, z: currentZPosition});
            }
        }

        function verwijderObject(object){
            object.setAttribute("visible", "false");
            object.classList.remove("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition > -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition - 100, z: currentZPosition});
            }
        }

        // Instellingen - Sound mute of on
        let mute_background_sound = () => {
            if (sound_icon.getAttribute("src") == "#sound-on") {
                background_sound.components.sound.stopSound();
                sound_icon.setAttribute("src", "#sound-mute");
            } else if (sound_icon.getAttribute("src") == "#sound-mute") {
                background_sound.components.sound.playSound();
                sound_icon.setAttribute("src", "#sound-on");
            }
        }
        sound_icon.addEventListener('click', mute_background_sound);

    }
});