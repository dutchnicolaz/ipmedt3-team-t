AFRAME.registerComponent('build', {

    // initialisatie van de basis functie
    init: function () {
        
        var click_sound = document.getElementById("js--click-sound");

        let onderdelen = document.querySelectorAll(".onderdelen");
        let onderdelen_doos = document.getElementById("js--part-box");
        let butHome = document.getElementById("js--home");
        let butBack = document.getElementById("js--back");
        const planken = document.querySelectorAll(".js--plank");

        let show_onderdelen = () => {
            onderdelen.forEach((parts) => {
                voegObjectToe(parts);
            });
            click_sound.components.sound.playSound();
        }
        onderdelen_doos.addEventListener('click', show_onderdelen);

        let remove_onderdelen = () => {
            onderdelen.forEach((parts) => {
                verwijderObject(parts);
                for (var i = 0; i < planken.length; i++) {
                    planken[i].setAttribute("visible", "false");
                }
            });
        }
        butHome.addEventListener('click', remove_onderdelen);
        butBack.addEventListener('click', remove_onderdelen);

        function voegObjectToe(object){
            object.setAttribute("visible", "true");
            object.classList.add("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition < -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition + 100, z: currentZPosition});
            }
        }
    
        function verwijderObject(object){
            object.setAttribute("visible", "false");
            object.classList.remove("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition > -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition - 100, z: currentZPosition});
            }
        }
    }
});