AFRAME.registerComponent('steps_screen_hyllis', {

    // initialisatie van de basis functie
    init: function () {

        let stepsHyllis = document.querySelectorAll(".steps_hyllis");
        let terugKnop = document.getElementById("js--terug-knop-hyllis");
        let butHome = document.getElementById("js--home-hyllis");
        let butBack = document.getElementById("js--back-hyllis");
        let startScherm = document.querySelectorAll(".meubelKeuzeScherm")
        let buildKnoppen = document.querySelectorAll(".bouwen-hyllis");
        let instructie = document.querySelectorAll(".bouwinstructie");

        // Sounds
        var click_sound = document.getElementById("js--click-sound");

        let meubel = document.getElementById("js--meubel");
        const clickBox = document.getElementById("js--clickbox");
        const kallaxBox = document.getElementById("js--kallax-clickbox");
        const hyllisBox = document.getElementById("js--hyllis-clickbox");

        // Stappen
        let hyllis_start = document.querySelectorAll(".hyllis-start");

        // Functie die het stappen scherm toont van Hyllis
        let show_step_hyllis = () => {
            // Na klikken op de box toon stappen van Hyllis kast
            stepsHyllis.forEach((hyllis) => {
                voegObjectToe(hyllis);
            });
            remove_start_screen();
            remove_build_screen();
            click_sound.components.sound.playSound();
        }
        this.el.addEventListener('click', show_step_hyllis);

        // Functie die het stappen scherm van Hyllis laat verdwijnen
        let remove_steps_hyllis = () => {
            stepsHyllis.forEach((hyllis) => {
                verwijderObject(hyllis);
            });
        }

        // Toon het start scherm weer met keuze meubels
        let show_start_screen = () => {
            startScherm.forEach((start) => {
                voegObjectToe(start);
            });
        }

        // Toon het start scherm weer met keuze meubels
        let remove_start_screen = () => {
            startScherm.forEach((start) => {
                verwijderObject(start);
            });
        }

        // Toon het build scherm
        let show_build_screen = () => {
            buildKnoppen.forEach((knoppen) => {
                voegObjectToe(knoppen);
            });
        }

        // Verwijder het build scherm
        let remove_build_screen = () => {
            buildKnoppen.forEach((knoppen) => {
                verwijderObject(knoppen);
            });
        }

        // Toon de instructie stappen
        let show_instructie = () => {
            instructie.forEach((stapjes) => {
                voegObjectToe(stapjes);
            });
        }

        // Verwijder de instructie stappen
        let remove_instructie = () => {
            instructie.forEach((stapjes) => {
                verwijderObject(stapjes);
            });
        }

        // Functie om terug te gaan van stap scherm naar product keuze scherm
        let terugKnopFunctie = () => {
            stepsHyllis.forEach((hyllis) => {
                verwijderObject(hyllis);
            });
            show_start_screen();
            remove_build_screen();
            click_sound.components.sound.playSound();
            verwijderObject(clickBox);
            voegObjectToe(meubel);
            voegObjectToe(hyllisBox);
            verwijderObject(kallaxBox);
        }
        terugKnop.addEventListener('click', terugKnopFunctie);

        // Wanneer je op stap 1 klikt
        let hyllisStapEenFunctie = () => {
            // Toon de bouw knoppen
            show_build_screen();
            remove_steps_hyllis();
            show_instructie();
            click_sound.components.sound.playSound();
        }

        for (var i = 0; i < hyllis_start.length; i++) {
            hyllis_start[i].addEventListener('click', hyllisStapEenFunctie);
        }

        // Terug naar stappenscherm vanuit build
        let backButtonFunctie = () => {
            show_step_hyllis();
            remove_build_screen();
            remove_instructie();
            click_sound.components.sound.playSound();
        }

        // Terug naar beginscherm vanuit build
        let homeButtonFunctie = () => {
            show_start_screen();
            remove_build_screen();
            voegObjectToe(meubel);
            verwijderObject(clickBox);
            click_sound.components.sound.playSound();
        }

        butBack.addEventListener('click', backButtonFunctie);
        butHome.addEventListener('click', homeButtonFunctie);

        // Voeg een model object toe
        function voegObjectToe(object){
            object.setAttribute("visible", "true");
            object.classList.add("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition < -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition + 100, z: currentZPosition});
            }
        }

        // Verwijder een model object
        function verwijderObject(object){
            object.setAttribute("visible", "false");
            object.classList.remove("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition > -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition - 100, z: currentZPosition});
            }
        }
    }
});