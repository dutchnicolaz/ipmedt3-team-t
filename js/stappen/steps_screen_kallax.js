AFRAME.registerComponent('steps_screen_kallax', {

    // initialisatie van de basis functie
    init: function () {

        let stepsKallax = document.querySelectorAll(".steps_kallax");
        let terugKnop = document.getElementById("js--terug-knop");
        let butHome = document.getElementById("js--home");
        let butBack = document.getElementById("js--back");
        let buildKnoppen = document.querySelectorAll(".bouwen");
        let startScherm = document.querySelectorAll(".meubelKeuzeScherm")
        let instructie = document.querySelectorAll(".bouwinstructie");
        let knopInstellingen = document.getElementById("js--knop-instellingen");
        let schermInstellingen = document.getElementById("js--scherm-instellingen")
        let classInstellingen = document.querySelectorAll(".instellingen");
        let kruisjeInstellingen = document.getElementById("js--kruisje-instellingen");

        var click_sound = document.getElementById("js--click-sound");

        let meubel = document.getElementById("js--meubel");
        const clickBox = document.getElementById("js--clickbox");
        const kallaxBox = document.getElementById("js--kallax-clickbox");
        const hyllisBox = document.getElementById("js--hyllis-clickbox");

        // Stappen
        let kallax_start = document.querySelectorAll(".kallax-start");

        // Functie die het stappen scherm toont
        let show_step_kallax = () => {
            // Na klikken op de box toon stappen van kallax kast
            stepsKallax.forEach((kallax) => {
                voegObjectToe(kallax);
            });
            remove_start_screen();
            remove_build_screen();
            click_sound.components.sound.playSound();
        }
        this.el.addEventListener('click', show_step_kallax);

        // Functie die het stappen scherm laat verdwijnen
        let remove_steps_kallax = () => {
            stepsKallax.forEach((kallax) => {
                verwijderObject(kallax);
            });
        }

        // Toon instellingen scherm
        let show_instellingen = () => {
            classInstellingen.forEach((scherm) => {
                voegObjectToe(scherm);
            });
            schermInstellingen.setAttribute("position", "0 2 -5.5");
            click_sound.components.sound.playSound();
        }
        knopInstellingen.addEventListener('click', show_instellingen);

        // Verwijder instellingen scherm
        let remove_instellingen = () => {
            classInstellingen.forEach((scherm) => {
                verwijderObject(scherm);
            });
            schermInstellingen.setAttribute("position", "0 10 -5.5");
            click_sound.components.sound.playSound();
        }
        kruisjeInstellingen.addEventListener('click', remove_instellingen);

        // Toon het start scherm weer met keuze meubels
        let show_start_screen = () => {
            startScherm.forEach((start) => {
                voegObjectToe(start);
            });
        }

        // Toon het start scherm weer met keuze meubels
        let remove_start_screen = () => {
            startScherm.forEach((start) => {
                verwijderObject(start);
            });
        }

        // Toon het build scherm
        let show_build_screen = () => {
            buildKnoppen.forEach((knoppen) => {
                voegObjectToe(knoppen);
            });
        }

        // Verwijder het build scherm
        let remove_build_screen = () => {
            buildKnoppen.forEach((knoppen) => {
                verwijderObject(knoppen);
            });
        }

        // Toon de instructie stappen
        let show_instructie = () => {
            instructie.forEach((stapjes) => {
                voegObjectToe(stapjes);
            });
        }

        // Verwijder de instructie stappen
        let remove_instructie = () => {
            instructie.forEach((stapjes) => {
                verwijderObject(stapjes);
            });
        }

        // Functie om terug te gaan van stap scherm naar product keuze scherm
        let terugKnopFunctie = () => {
            stepsKallax.forEach((kallax) => {
                verwijderObject(kallax);
            });
            show_start_screen();
            remove_build_screen();
            click_sound.components.sound.playSound();
            verwijderObject(clickBox);
            voegObjectToe(meubel);
            voegObjectToe(kallaxBox);
            verwijderObject(hyllisBox);
        }
        terugKnop.addEventListener('click', terugKnopFunctie);

        // Wanneer je op stap 1 klikt
        let kallaxStapEenFunctie = () => {
            // Toon de bouw knoppen
            show_build_screen();
            remove_steps_kallax();
            show_instructie();
            click_sound.components.sound.playSound();
        }

        for (var i = 0; i < kallax_start.length; i++) {
            kallax_start[i].addEventListener('click', kallaxStapEenFunctie);
        }

        // Terug naar stappenscherm vanuit build
        let backButtonFunctie = () => {
            show_step_kallax();
            remove_build_screen();
            remove_instructie();
            click_sound.components.sound.playSound();
        }

        // Terug naar beginscherm vanuit build
        let homeButtonFunctie = () => {
            show_start_screen();
            remove_build_screen();
            voegObjectToe(meubel);
            verwijderObject(clickBox);
            click_sound.components.sound.playSound();
        }

        butBack.addEventListener('click', backButtonFunctie);
        butHome.addEventListener('click', homeButtonFunctie);

        function voegObjectToe(object){
            object.setAttribute("visible", "true");
            object.classList.add("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition < -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition + 100, z: currentZPosition});
            }
        }
    
        function verwijderObject(object){
            object.setAttribute("visible", "false");
            object.classList.remove("clickable");
            let currentXPosition = object.getAttribute("position").x;
            let currentYPosition = object.getAttribute("position").y;
            let currentZPosition = object.getAttribute("position").z;
            if (currentYPosition > -50){
                object.setAttribute("position", {x: currentXPosition, y: currentYPosition - 100, z: currentZPosition});
            }
        }
    }
});