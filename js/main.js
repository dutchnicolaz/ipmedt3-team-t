window.onload = function() {
    // ELEMENT ID
    const information_text = document.getElementById("js--information-text");
    const rotate_mode = document.getElementById("js--rotate-mode");
    const select_mode = document.getElementById("js--select-mode");

    const stappenScherm = document.getElementById("js-stappenscherm")

    var turnMode = false;
    var selectMode = true;

    // ROTATE MODUS
    rotate_mode.addEventListener('click', function(e) {
        turnMode = true;
        information_text.setAttribute('value', "Rotatie modus")
    });

    // SELECT MODUS
    select_mode.addEventListener('click', function(e) {
        turnMode = false;
        selectMode = true;
        information_text.setAttribute('value', "Selecteer modus");
    });

    function voegObjectToe(object){
        object.setAttribute("visible", "true");
        object.classList.add("clickable");
        let currentXPosition = object.getAttribute("position").x;
        let currentYPosition = object.getAttribute("position").y;
        let currentZPosition = object.getAttribute("position").z;
        if (currentYPosition < -50){
            object.setAttribute("position", {x: currentXPosition, y: currentYPosition + 100, z: currentZPosition});
        }
    }

    function verwijderObject(object){
        object.setAttribute("visible", "false");
        object.classList.remove("clickable");
        let currentXPosition = object.getAttribute("position").x;
        let currentYPosition = object.getAttribute("position").y;
        let currentZPosition = object.getAttribute("position").z;
        if (currentYPosition > -50){
            object.setAttribute("position", {x: currentXPosition, y: currentYPosition - 100, z: currentZPosition});
        }
    }


    helpButton.addEventListener('click', function(e) {
        voegObjectToe(helpMenu);
        helpMenu.setAttribute("position", "0 0 -2");
    })

}